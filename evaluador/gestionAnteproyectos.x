/*Declaracion de datos a transferir entre el cliente y el servidor*/
/*Declaracion de constantes*/
const MAXNOM = 30;
const MAXCON = 20;
const MAXTIT = 40;
const MAXDATO= 10;

typedef struct nodo_anteproyecto *proxNodoAnteproyecto;

/*Declaracion de la estructura que permite almacenar los datos de un anteproyecto*/

struct nodo_anteproyecto {
	char modalidad[MAXTIT];
	char titulo[MAXTIT];
	int codigo;
	char nombre_estud1[MAXNOM];
	char nombre_estud2[MAXNOM];
	char nombre_dir[MAXNOM];
	char nombre_codir[MAXNOM];
	char fecha_registro[MAXDATO];
	int concepto;
	int estado;
	int num_revision;
	char fecha_aprobacion[MAXDATO];
	proxNodoAnteproyecto nodoSiguienteA;/*Atributo del nodo anteproyecto que le permite guardar la referencia al siguiente nodo*/
};

struct nodo_evaluadores {
	char codigo[MAXDATO]; 
	char nombre_eval1[MAXNOM];
	char con_eval1[MAXCON];
	char fecha_r1[MAXDATO];
	char nombre_eval2[MAXNOM];
	char con_eval2[MAXCON];
	char fecha_r2[MAXDATO];
};

struct nodo_info {
	nodo_anteproyecto nodoAnteproyecto;
	nodo_evaluadores nodoEval;
};

struct concepto {
	char codigo[MAXDATO];
	int concepto;   
   	char nombre_eval[MAXNOM];

};

/*Definicion de las operaciones que se pueden realizar*/
program gestion_anteproyectos{
	version gestion_anteproyectos_version {
		bool registrarAnteproyecto(nodo_anteproyecto objAnteproyecto)=1;
		bool asignarEvaluadores(nodo_evaluadores objAnteproyecto)=2;
		nodo_info consultarAnteproyecto(int codigo)=3;
		proxNodoAnteproyecto listarAnteproyectos(void)=4;
        bool modificarConcepto(int codigo)=5;
        bool ingresarConcepto(concepto con)=6;
	}=2;
}=0x20000001;
